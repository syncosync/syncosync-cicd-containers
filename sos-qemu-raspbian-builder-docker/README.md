This is the dockerfile for running the CustomPiOS project to build our very own syncosync Raspbian port.

to use them locally issue:

docker build -t sos-qemu-raspbian-builder .

to use them for the CI/CD on gitlab:

docker build -t registry.gitlab.com/syncosync/syncosync/sos-qemu-raspbian-builder .
docker push registry.gitlab.com/syncosync/syncosync/sos-qemu-raspbian-builder
